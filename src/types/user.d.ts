export namespace API {
    export interface ResetPassword {
        old: string
        new: string
        new2: string
    }

    export interface Resp {
        message: string
        code: number
    }
}
