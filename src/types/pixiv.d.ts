export namespace API {
    export type Model = "day" | "week" | "month"
    export type Genre = "manga" | "illust"
    export interface ListInfo {
        adId?: number
        id: number
        pageCount: number
        title: string
        type: Genre
        artistPreView: {
            avatar: string
            id: number
            name: string
        }
        imageUrls: Array<{
            large: string
            medium: string
            original: string
            squareMedium: string
        }>
    }

    export interface ListResponse {
        message: string
        data: ListInfo[]
    }
}
