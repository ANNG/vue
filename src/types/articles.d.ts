export namespace API {
    export interface BaseResp {
        code: number
        message: string
    }

    export interface ListInfo {
        main_img: string
        title: string
        id: number
    }

    export interface RespList {
        message: string
        code: number
        data: ListInfo[]
    }

    export namespace CATE {
        export interface Info {
            title: string
            total: number
            id: number
        }

        export namespace RESP {
            export type Lists = BaseResp & { data: Info[] }
        }
    }
}
