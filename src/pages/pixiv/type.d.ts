import type { API } from "@/types/pixiv"
import type MFiles from "./method/files"
import type { Ref } from "vue"

type TProgressType = "progress" | "end"

type TModel = API.Model

type TModelRanks = Record<
    TModel,
    (data: string, page: number) => Promise<API.ListResponse>
>

interface TImgs {
    adId?: number
    url: string
    loading?: boolean
    progress?: TProgressType
    id: number
    is_download?: boolean
    title: string
    num: number
    type: API.Genre
    visible?: boolean
    author?: {
        name: string
        avatar: string
    }
}

type TFileProgress = Map<number, Ref<{ state: string; progress: number }>>
