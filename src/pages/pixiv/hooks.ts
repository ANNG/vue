import { ref, type Ref, watchEffect, reactive } from "vue"
import type { TImgs, TModel, TModelRanks, TFileProgress } from "./type"
import moment from "moment"
/** API */
import { RanksDay, RanksWeek, RanksMonth } from "@/api/pxivi"

/* ---------------------------------- Type ---------------------------------- */
type actionTypes = "new" | "next" | "pre"

export const useModel = (
    sourceData: Ref<TImgs[]>,
    loading: Ref<boolean>,
): {
    page: Ref<number>
    model: Ref<TModel>
    time: Ref<string>
    action: Record<actionTypes, () => void>
    state: Record<string, boolean>
} => {
    const model = ref<TModel>("day")
    let modelCache = model.value
    const newData = moment().subtract(2, "day").format("YYYY-MM-DD")
    const time = ref<string>(newData)
    const page = ref<number>(1)
    const state = reactive<Record<string, boolean>>({
        new: true,
        next: false,
    })

    // const momentModel: Record<TModel, moment.unitOfTime.DurationConstructor> = {
    //     day: "d",
    //     week: "w",
    //     month: "M",
    // }

    const api: TModelRanks = {
        day: RanksDay,
        week: RanksWeek,
        month: RanksMonth,
    }

    const action: Record<actionTypes, () => void> = {
        new: () => {
            time.value = newData
        },
        next: () => {
            time.value = moment(time.value).add(1, "d").format("YYYY-MM-DD")
        },
        pre: () => {
            time.value = moment(time.value)
                .subtract(1, "d")
                .format("YYYY-MM-DD")
        },
    }

    watchEffect(async () => {
        if (modelCache !== model.value) {
            page.value = 1
            modelCache = model.value
        }

        state.new = moment(time.value).isSame(newData)
        state.next = moment(newData).isSameOrBefore(time.value)
        loading.value = true
        const { data } = await api[model.value](time.value, page.value)

        sourceData.value = [
            ...(page.value > 1 ? sourceData.value : []),
            ...data
                .map((item): TImgs => {
                    return {
                        adId: item.adId,
                        id: item.id,
                        url: item.imageUrls[0].medium.replace(
                            "https://i.pximg.net",
                            "/pixiv",
                        ),
                        loading: false,
                        type: item.type,
                        is_download: false,
                        title: item.title,
                        num: item.pageCount,
                        author: {
                            name: item.artistPreView.name,
                            avatar: item.artistPreView.avatar.replace(
                                "https://i.pximg.net",
                                "/pixiv",
                            ),
                        },
                    }
                })
                .filter((item) => item.adId === undefined),
        ]
        loading.value = false
    })

    return { page, model, time, state, action }
}

export const useProgress = (): {
    progress: TFileProgress
} => {
    const progress = reactive<TFileProgress>(new Map())
    const d = ref({ state: "end", progress: 10 })
    progress.set(100, d)
    watchEffect(() => {
        progress.forEach((item) => {
            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            item.value
        })
    })

    return { progress }
}
