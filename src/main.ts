import { createApp } from "vue"
import "./style.css"
import App from "./App.vue"
import "@mdi/font/css/materialdesignicons.css"

// Vuetify
import "vuetify/styles"
import { createVuetify } from "vuetify"
import * as components from "vuetify/components"
import * as directives from "vuetify/directives"
import { aliases, mdi } from "vuetify/iconsets/mdi"
import * as VData from "vuetify/labs/VDatePicker"
import { pl, zhHans } from "vuetify/locale"

// 时间插件
import VueDatePicker from "@vuepic/vue-datepicker"
import "@vuepic/vue-datepicker/dist/main.css"

// 路由
import Router from "@/routes"

// el-ui
import ElementPlus from "element-plus"
import "element-plus/dist/index.css"
import "element-plus/theme-chalk/dark/css-vars.css"

// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from "@element-plus/icons-vue"

// 全局组件 global
import GlobalLayout from "@/components/layout/Index.vue"

const vuetify = createVuetify({
    icons: {
        defaultSet: "mdi",
        aliases,
        sets: {
            mdi,
        },
    },
    theme: {
        defaultTheme: "dark",
    },
    // eslint-disable-next-line no-import-assign
    components: { ...components, ...VData },
    directives,
    locale: {
        locale: "zhHans",
        messages: { zhHans, pl },
    },
})

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app.use(Router)
    .use(ElementPlus)
    .component("VueDatePicker", VueDatePicker)
    .component("gl-layout", GlobalLayout)
    .use(vuetify)
    .mount("#app")
