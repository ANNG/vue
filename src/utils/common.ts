// 防抖函数
export const debounce = (): ((fn: () => void) => void) => {
    let timeId: NodeJS.Timeout | null = null
    return (fn: () => void): void => {
        if (timeId !== null) {
            clearTimeout(timeId)
        }
        timeId = setTimeout(() => {
            fn()
        }, 500)
    }
}

export const p = <T>(fn: Promise<T>): T | null => {
    let cache: T | null = null
    const func = (): void => {
        if (cache != null) {
            return
        }

        const promise = fn.then((data) => {
            cache = data
            console.log(100)
        })

        throw promise
    }

    try {
        func()
    } catch (error) {
        if (error instanceof Promise) {
            error.then(func)
        }
    }

    return cache
}
