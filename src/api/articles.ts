import type { API } from "@/types/articles"
import request from "@/utils/request"

request.interceptors.response.use((response) => {
    return response.data
})

export const GetLists = (): Promise<API.RespList> =>
    request({
        url: "/articles",
        method: "GET",
    })

export const CateGetLists = (): Promise<API.CATE.RESP.Lists> =>
    request({
        url: "/articles/cate",
        method: "GET",
    })

export const CateAdd = (name: string): Promise<API.BaseResp> =>
    request({
        url: "/articles/cate",
        method: "POST",
        data: { name },
    })

export const CateEdit = (id: number, name: string): Promise<API.BaseResp> =>
    request({
        url: "/articles/cate",
        method: "PUT",
        data: { name, id },
    })

export const CateDelete = (id: number = 1): Promise<API.BaseResp> =>
    request({
        url: "/articles/cate/" + id,
        method: "DELETE",
    })
