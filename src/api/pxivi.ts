import axios from "axios"

import type { API } from "@/types/pixiv"

const request = axios.create({})

const rank = [
    "https://www.pixivs.cn/ranking.php?p=2&mode=monthly&date=20231014&format=json",
]

request.interceptors.response.use((response) => {
    return response.data
})

type mode = "day" | "week" | "month"

export interface ListResponse {
    message: string
    data: API.ListInfo[]
}

// https://api.bbmang.me/ranks?mode=day&data=2023-10-09&page=1&pageSize=30
// https://api.bbmang.me/ranks?page=1&date=2023-10-09&mode=day&pageSize=30
const ranks = (
    mode: mode,
    date: string,
    page: number,
    pageSize: number = 30,
): Promise<ListResponse> =>
    request.get("https://api.bbmang.me/ranks", {
        params: { mode, date, page, pageSize },
    })

export const RanksDay = (date: string, page: number): Promise<ListResponse> =>
    ranks("day", date, page)

export const RanksWeek = (data: string, page: number): Promise<ListResponse> =>
    ranks("week", data, page)

export const RanksMonth = (data: string, page: number): Promise<ListResponse> =>
    ranks("month", data, page)

export default (): void => {
    console.log(100)
}
