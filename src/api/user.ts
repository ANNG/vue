import axios from "axios"
import type { API } from "@/types/user"

const request = axios.create({})

request.interceptors.response.use((response) => {
    return response.data
})
export const ResetPassword = (data: API.ResetPassword): Promise<API.Resp> =>
    request({
        url: "/api/user/resetPassword",
        method: "POST",
        data,
    })
