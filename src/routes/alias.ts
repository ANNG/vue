const alias: Record<string, string> = {
    pixivRank: "Pixiv榜单",
    config: "网站配置",
    configInfo: "个人信息",
}

export default alias
