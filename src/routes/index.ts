import { createRouter, createWebHistory, type RouteRecordRaw } from "vue-router"

export const routes: RouteRecordRaw[] = [
    {
        path: "/",
        name: "home",
        component: () => import("@/pages/users/Index.vue"),
    },
    {
        path: "/config",
        name: "config",
        children: [
            {
                path: "info",
                name: "configInfo",
                component: () => import("@/pages/users/Index.vue"),
            },
        ],
    },
    {
        path: "/articles",
        name: "articles",
        children: [
            {
                path: "",
                name: "articlesLists",
                component: () => import("@/pages/articles/Index.vue"),
            },
            {
                path: "add",
                name: "articlesAdd",
                component: () => import("@/pages/articles/Add.vue"),
            },
            {
                path: "cate",
                name: "articleCate",
                component: () => import("@/pages/articles/cate/index.vue"),
            },
        ],
    },
    {
        path: "/lists",
        name: "pixiv",
        children: [
            {
                path: "rank",
                name: "pixivRank",
                alias: "Pixiv榜单",
                component: () => import("@/pages/pixiv/Index.vue"),
            },
        ],
    },
]

const router = createRouter({
    routes,
    history: createWebHistory(),
})

export default router
