import { type MockOptions, defineMock } from "vite-plugin-mock-dev-server"
import Mock from "mockjs"

const cate: MockOptions = [
    {
        url: "/api/articles/cate",
        method: "GET",
        body: Mock.mock({
            code: 200,
            message: "查询成功",
            "data|1-10": [
                {
                    "id|+1": 1,
                    title: Mock.mock("@ctitle"),
                    total: 10,
                    color: Mock.mock("@color"),
                },
            ],
        }),
    },
    {
        url: "/api/articles/cate",
        delay: 1000,
        method: "POST",
        body: Mock.mock({
            code: 200,
            message: "添加成功",
        }),
    },
    {
        url: "/api/articles/cate",
        delay: 1000,
        method: "PUT",
        body: Mock.mock({
            code: 200,
            message: "编辑成功",
        }),
    },
    {
        url: "/api/articles/cate/1",
        delay: 1000,
        method: "DELETE",
        body: Mock.mock({
            code: 200,
            message: "编辑成功",
        }),
    },
]

export default defineMock([
    {
        url: "/api/articles",
        method: "GET",
        body: Mock.mock({
            code: 200,
            message: "查询成功",
            "data|1-10": [
                {
                    "id|+1": 1,
                    main_img:
                        "/pixiv/c/540x540_70/img-master/img/2023/10/19/00/00/16/112660887_p0_master1200.jpg",
                    title: Mock.mock("@ctitle"),
                },
            ],
        }),
    },
    ...cate,
])
