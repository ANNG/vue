import { defineMock } from "vite-plugin-mock-dev-server"

export default defineMock({
    url: "/api/user/resetPassword",
    method: "POST",
    delay: 1000,
    response: (req, res) => {
        const { body } = req
        res.statusCode = 200
        if (body.new !== body.new2) {
            res.end(
                JSON.stringify({
                    message: "密码不一样",
                    code: 400,
                }),
            )
        } else {
            res.end(
                JSON.stringify({
                    message: "修改成功",
                    code: 200,
                }),
            )
        }
    },
})
