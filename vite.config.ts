import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import path from "path"
import mockDevServerPlugin from "vite-plugin-mock-dev-server"

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        mockDevServerPlugin()
    ],
    server: {
        proxy: {
            "^/api": {
                target: "http://127.0.0.1",
            },
            "/pixiv": {
                target: "https://i.pixiv.re",
                headers: {
                    Referer: "https://www.pixiv.net/",
                },
                // 关闭 https证书验证
                changeOrigin: true,
                secure: false,
                rewrite: (path) => path.replace(/^\/pixiv/, ""),
            },
        },
    },
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
    },
})
