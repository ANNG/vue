module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        "standard-with-typescript",
        "plugin:vue/vue3-essential",
        "prettier",
        "plugin:@typescript-eslint/disable-type-checked",
    ],
    overrides: [
        {
            env: {
                node: true,
            },
            files: [".eslintrc.{js,cjs}", "*.vue"],
            parserOptions: {
                sourceType: "script",
            },
        },
    ],
    parser: "vue-eslint-parser",
    parserOptions: {
        ecmaVersion: "latest",
        parser: "@typescript-eslint/parser",
        sourceType: "module",
        extraFileExtensions: [".vue"],
        project: "./tsconfig.json",
    },
    plugins: ["vue", "prettier", "@typescript-eslint"],
    rules: {
        "prettier/prettier": "error",
        "@typescript-eslint/semi": "error",
        "@typescript-eslint/no-unused-vars": "off",
        "vue/multi-word-component-names": "off",
        // "vue/multi-word-component-names": [
        //     "error",
        //     {
        //         ignores: ["index", "main","Index"],
        //     },
        // ],
    },
}
